package org.guess.showcase.qixiu.service;

import org.guess.core.service.BaseService;
import org.guess.showcase.qixiu.model.Record;

/**
 * 记录service
 * @author rguess
 * @version 2014-06-04
 */
public interface RecordService extends BaseService<Record, Long>{

}
